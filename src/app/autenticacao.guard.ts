import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { AutenticacaoService } from './servicos/autenticacao/autenticacao.service';

@Injectable({
  providedIn: 'root'
})
export class AutenticacaoGuard implements CanActivate, CanActivateChild {

  constructor(private autenticacaoServico: AutenticacaoService, private router: Router){
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean{
      var statusAutenticacao = this.autenticacaoServico.checkCookieAutenticacao();
      if(!statusAutenticacao){
        this.router.navigate(["/login"]);
      }
      return statusAutenticacao;

  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

      return this.canActivate(next, state);
  }
}

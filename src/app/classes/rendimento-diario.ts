import { Veiculo } from '../interfaces/veiculo';
import { Aplicativo } from '../interfaces/aplicativo';
import { Diaria } from './diaria';

export class RendimentoDiario {
  idRendimentoDiario: number;
  veiculo: Veiculo;
  aplicativo: Aplicativo;
  tempoEmCorrida: string;
  distanciaPercorridaEmKm: number;
  valorRecebido: number;
  diaria: Diaria;

  constructor(){
    this.veiculo = {} as Veiculo;
    this.aplicativo = {} as Aplicativo;
    this.diaria = new Diaria();
  }

  
}

import { Aplicativo } from '../interfaces/aplicativo';

export class Faturamento {
  aplicativo: Aplicativo;
  distanciaPercorrida: number;
  tempoEmCorrida: string;
  valor: number;
  periodo: string;
}

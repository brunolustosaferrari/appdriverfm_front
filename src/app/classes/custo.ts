import { TipoCusto } from './tipo-custo';
import { Parcela } from './parcela';
import { Veiculo } from '../interfaces/veiculo';

export class Custo {

  idCusto: number;
  data: Date;
  tipo: TipoCusto;
  parcelas: Parcela[];
  titulo: string;
  descricao: string;
  veiculo: Veiculo;

  constructor(){
    this.data = new Date();
    this.tipo = new TipoCusto();
    this.parcelas = new Array<Parcela>();
    this.veiculo = {} as Veiculo;
  }
}

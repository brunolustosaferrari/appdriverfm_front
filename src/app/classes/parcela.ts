import { Custo } from './custo';

export class Parcela {
  idParcela: number;
  dataVencimento: Date;
  valor: number;
  custo: Custo;
  pago: boolean;

  constructor(){
    this.dataVencimento = new Date();
    this.custo = new Custo();
  }
}

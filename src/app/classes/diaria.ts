import { RendimentoDiario } from './rendimento-diario';

export class Diaria {
  idDiaria: number;
  dataHoraInicio: Date;
  dataHoraFim: Date;
  rendimentosDiarios: RendimentoDiario[];

  constructor(){
    this.dataHoraFim = new Date(0);
    this.dataHoraInicio = new Date(0);
    this.rendimentosDiarios = new Array<RendimentoDiario>();
  }
}

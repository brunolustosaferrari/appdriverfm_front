export interface Aplicativo {

  idAplicativo: number;
  nome: string;
  descricao: string;
}

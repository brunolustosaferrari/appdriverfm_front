export interface Veiculo {

  idVeiculo: number;
  placa: string;
  nome: string;
  descricao: string;

}

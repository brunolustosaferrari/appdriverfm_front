import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Aplicativo } from 'src/app/interfaces/aplicativo';
import { environment } from 'src/environments/environment';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AplicativoService {

  private urlRecurso: string = environment.apiUri + "aplicativo/";

  constructor(private http: HttpClient) { }

  buscar(id:number){
    return this.http.get<Aplicativo>(this.urlRecurso + id.toString())
      .pipe(catchError(this.manipularErro));
  }

  inserir(aplicativo: Aplicativo){
    return this.http.post(this.urlRecurso, aplicativo).pipe(catchError(this.manipularErro));
  }

  atualizar(aplicativo: Aplicativo){
    return this.http.put(this.urlRecurso, JSON.stringify(aplicativo),
      {headers: new HttpHeaders().append("Content-Type", "application/json")})
      .pipe(catchError(this.manipularErro));
  }

  excluir(idAplicativo:number){
    return this.http.delete(this.urlRecurso + idAplicativo.toString())
      .pipe(catchError(this.manipularErro));
  }

  listar(){
    return this.http.get(this.urlRecurso).pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;

    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }
}

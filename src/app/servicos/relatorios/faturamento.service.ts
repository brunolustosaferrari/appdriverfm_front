import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Faturamento } from 'src/app/classes/faturamento';

@Injectable()
export class FaturamentoService {

  private urlRecurso = environment.apiUri + "faturamento/";

  constructor(private http: HttpClient) { }

  buscarPorAno(ano: string){
    return this.http.get(this.urlRecurso + ano)
      .pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;

    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }
}

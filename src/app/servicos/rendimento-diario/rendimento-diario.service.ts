import { Injectable } from '@angular/core';
import { RendimentoDiario } from 'src/app/classes/rendimento-diario';

@Injectable({
  providedIn: 'root'
})
export class RendimentoDiarioService {

  constructor() { }

  getValorPorHora(rendimento: RendimentoDiario){
    let horas:number = Number(rendimento.tempoEmCorrida.split(":")[0]);
    let minutos:number = horas * 60 + Number(rendimento.tempoEmCorrida.split(":")[1]);
    return ((rendimento.valorRecebido / minutos) * 60);
  }

  getValorPorDistancia(rendimento: RendimentoDiario){
    return rendimento.valorRecebido / rendimento.distanciaPercorridaEmKm;
  }
}

import { TestBed } from '@angular/core/testing';

import { RendimentoDiarioService } from './rendimento-diario.service';

describe('RendimentoDiarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RendimentoDiarioService = TestBed.get(RendimentoDiarioService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Usuario } from 'src/app/interfaces/usuario';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CookieService } from 'angular2-cookie';

@Injectable()
export class AutenticacaoService {

  private urlRecurso = environment.apiUri + "autenticacao";

  constructor(private http: HttpClient, private cookieServico: CookieService) { }

  autenticar(usuario: Usuario): Observable<string>{
    let uri = this.urlRecurso + "/" + usuario.email + "/" + usuario.senha;
    return this.http.get<string>(uri)
      .pipe(catchError(this.manipularErro));
  }

  private manipularErro(erro: HttpErrorResponse){
    var statusErro = erro.status;
    var mensagem: String;

    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{

      switch(statusErro){
        case 500:
          mensagem = "Erro interno no servidor. Tente novamente mais tarde.";
          break;

        default:
          mensagem = "Acesso negado. Confire seu email e sua senha.";
          break;
      }

    }

    return throwError(mensagem);
  }

  checkCookieAutenticacao(): boolean{
    if(this.cookieServico.get("tokenAutenticacao") != undefined){
      return true;
    }
    return false;
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { TipoCusto } from 'src/app/classes/tipo-custo';

@Injectable()
export class TipoCustoService {

  private urlRecurso = environment.apiUri + "tipo-custo/";

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get(this.urlRecurso)
    .pipe(catchError(this.manipularErro));
  }

  inserir(tipo: TipoCusto){
    return this.http.post(this.urlRecurso, tipo).pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;

    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }
}

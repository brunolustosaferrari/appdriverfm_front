import { TestBed } from '@angular/core/testing';

import { CustoService } from './custo.service';

describe('CustoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustoService = TestBed.get(CustoService);
    expect(service).toBeTruthy();
  });
});

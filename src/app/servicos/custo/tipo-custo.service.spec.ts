import { TestBed } from '@angular/core/testing';

import { TipoCustoService } from './tipo-custo.service';

describe('TipoCustoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoCustoService = TestBed.get(TipoCustoService);
    expect(service).toBeTruthy();
  });
});

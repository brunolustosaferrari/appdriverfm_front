import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { Parcela } from 'src/app/classes/parcela';

@Injectable()
export class ParcelaService {

  constructor(private http: HttpClient) { }

  private urlRecurso = environment.apiUri + "parcela/";

  listarEmAberto(pagina?:number, tamanhoPagina?:number){
    if(tamanhoPagina == undefined) tamanhoPagina = 10;
    if(pagina == undefined) pagina = 0;
    return this.http.get( this.urlRecurso + pagina.toString() + "/" + tamanhoPagina.toString() )
    .pipe(catchError(this.manipularErro));
  }

  atualizar(parcela:Parcela){
    return this.http.put(this.urlRecurso, JSON.stringify(parcela),
      {headers: new HttpHeaders().append("Content-Type", "application/json")})
      .pipe(catchError(this.manipularErro));
  }

  buscarContagemParcelas(){
    return this.http.get(this.urlRecurso).pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;
    console.log(erro.message);
    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }

}

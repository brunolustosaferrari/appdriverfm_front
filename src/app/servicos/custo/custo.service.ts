import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Custo } from 'src/app/classes/custo';
import { throwError } from 'rxjs';

@Injectable()
export class CustoService {

  private urlRecurso:string = environment.apiUri + "custo/";

  constructor(private http:HttpClient) { }

  buscar(id:number){
    return this.http.get<Custo>(this.urlRecurso + id.toString())
      .pipe(catchError(this.manipularErro));
  }

  inserir(custo: Custo){
    return this.http.post(this.urlRecurso, custo).pipe(catchError(this.manipularErro));
  }

  atualizar(custo: Custo){
    return this.http.put(this.urlRecurso, JSON.stringify(custo),
      {headers: new HttpHeaders().append("Content-Type", "application/json")})
      .pipe(catchError(this.manipularErro));
  }

  excluir(idCusto:number){
    return this.http.delete(this.urlRecurso + idCusto.toString())
      .pipe(catchError(this.manipularErro));
  }

  listar(pagina?:number){
    let strPagina:string = "";
    if(pagina != undefined){
      strPagina = "pagina/" + pagina.toString();
    }
    return this.http.get(this.urlRecurso + strPagina).pipe(catchError(this.manipularErro));
  }

  buscarContagemPaginas(){
    return this.http.get(this.urlRecurso + "pagina").pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;
    console.log(erro.message);
    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }

}

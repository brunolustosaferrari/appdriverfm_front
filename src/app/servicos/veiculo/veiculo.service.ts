import { Injectable } from '@angular/core';
import { Veiculo } from 'src/app/interfaces/veiculo';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class VeiculoService {

  private urlRecurso = environment.apiUri + 'veiculo/';
  private opcoesHeader = new HttpHeaders().append("Content-Type", "application/json");
  constructor(private http: HttpClient) { }

  cadastrar(veiculo: Veiculo)  {
    return this.http.post(this.urlRecurso, veiculo)
      .pipe(catchError(this.manipularErro));
  }

  buscar(id: number){
    return this.http.get(this.urlRecurso + id)
    .pipe(catchError(this.manipularErro));
  }

  listar(){
    return this.http.get(this.urlRecurso)
    .pipe(catchError(this.manipularErro));
  }

  atualizar(veiculo: Veiculo)  {
    return this.http.put(this.urlRecurso, JSON.stringify(veiculo), {headers: this.opcoesHeader}  )
      .pipe(catchError(this.manipularErro));
  }

  excluir(veiculo: Veiculo)  {
    return this.http.delete(this.urlRecurso + veiculo.idVeiculo)
      .pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;

    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }
}

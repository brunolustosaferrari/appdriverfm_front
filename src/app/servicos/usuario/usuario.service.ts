import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Usuario } from 'src/app/interfaces/usuario';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

const httpOpcoesCadastro = {
  headers: new HttpHeaders(
    {
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:8080'
    }
  )
}


@Injectable()
export class UsuarioService {

  private urlRecurso = environment.apiUri + 'usuario';

  constructor(private http: HttpClient) { }

  cadastrar(usuario: Usuario){
    return this.http.post(this.urlRecurso, usuario, httpOpcoesCadastro)
      .pipe(catchError(this.manipularErro));
  }

  private manipularErro(erro: HttpErrorResponse){
    var statusErro = erro.status;
    var mensagem: String;

    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{

      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        case 409:
          mensagem = "Já existe um usuário com este email";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }

    }

    return throwError(mensagem);
  }
}

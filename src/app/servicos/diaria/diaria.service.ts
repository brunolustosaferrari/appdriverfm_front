import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { throwError } from 'rxjs';
import { Diaria } from 'src/app/classes/diaria';

@Injectable()
export class DiariaService {

  private urlRecurso: string = environment.apiUri + "diaria/";

  constructor(private http: HttpClient) { }

  buscar(id:number){
    return this.http.get<Diaria>(this.urlRecurso + id.toString())
      .pipe(catchError(this.manipularErro));
  }

  inserir(diaria: Diaria){
    return this.http.post(this.urlRecurso, diaria).pipe(catchError(this.manipularErro));
  }

  atualizar(diaria: Diaria){
    return this.http.put(this.urlRecurso, JSON.stringify(diaria),
      {headers: new HttpHeaders().append("Content-Type", "application/json")})
      .pipe(catchError(this.manipularErro));
  }

  excluir(idDiaria:number){
    return this.http.delete(this.urlRecurso + idDiaria.toString())
      .pipe(catchError(this.manipularErro));
  }

  listar(pagina?:number){
    let strPagina:string = "";
    if(pagina != undefined){
      strPagina = "pagina/" + pagina.toString();
    }
    return this.http.get(this.urlRecurso + strPagina).pipe(catchError(this.manipularErro));
  }

  buscarContagemPaginas(){
    return this.http.get(this.urlRecurso + "pagina").pipe(catchError(this.manipularErro));
  }

  listarSemana(){
    return this.http.get(this.urlRecurso + "ultima-semana").pipe(catchError(this.manipularErro));
  }

  listarMes(){
    return this.http.get(this.urlRecurso + "ultimo-mes").pipe(catchError(this.manipularErro));
  }

  manipularErro(erro: HttpErrorResponse){

    var statusErro = erro.status;
    var mensagem: String;
    console.log(erro.message);
    if(erro.error instanceof ErrorEvent){
      mensagem = "Erro local. Verifique sua conexão";
    }else{
      switch(statusErro){
        case 401:
        case 403:
        case 405:
          mensagem = "Você não possui permissão para acessar este recurso";
          break;

        default:
          mensagem = "Não foi possível concluir a requisição";
          break;
      }
    }
    return throwError(mensagem);
  }

  getDiaInicio(diaria: Diaria): string {
    return diaria.dataHoraInicio.toLocaleDateString();
  }

  getDiaFim(diaria: Diaria): string {
    return diaria.dataHoraFim.toLocaleDateString();
  }

  getDias(diaria: Diaria): string {
    let diaInicio = this.getDiaInicio(diaria);
    let diaFim = this.getDiaFim(diaria);
    return (diaInicio != diaFim) ? ( diaInicio + " - " + diaFim ) : diaInicio;
  }

  getHoraInicio(diaria: Diaria): string{
    return diaria.dataHoraInicio.toLocaleTimeString().substring(0,5);
  }

  getHoraFim(diaria: Diaria): string{
    return diaria.dataHoraFim.toLocaleTimeString().substring(0,5);
  }

  getTempoTotal(diaria: Diaria): string{
    let minutosFinal = diaria.dataHoraFim.getHours() * 60 + diaria.dataHoraFim.getMinutes();
    let minutosInicial = diaria.dataHoraInicio.getHours() * 60 + diaria.dataHoraInicio.getMinutes();
    let contHoras = Math.floor((minutosFinal - minutosInicial) / 60);
    let contMinutos = ((minutosFinal - minutosInicial) % 60) * 0.6;
    return contHoras.toString() + ":" + contMinutos.toString().padStart(2, "0");
  }

  getTempoTotalEmCorrida(diaria: Diaria): string{
    let minutosTotais:number = this.calcularTotalMinutosEmCorrida(diaria);
    let contHoras = Math.floor(minutosTotais / 60);
    let contMinutos = minutosTotais % 60;
    return contHoras.toString() + ":" + contMinutos.toString().padStart(2, "0") ;
  }

  getDistanciaTotal(diaria: Diaria): number{
    let distanciaTotal: number = 0;
    diaria.rendimentosDiarios.map((rendimento) => {
      distanciaTotal += rendimento.distanciaPercorridaEmKm;
    });
    return distanciaTotal;
  }

  getValorRecebidoTotal(diaria:Diaria): number{
    let valorRecebidoTotal: number = 0;
    diaria.rendimentosDiarios.map((rendimento) => {
      valorRecebidoTotal += rendimento.valorRecebido;
    });
    return valorRecebidoTotal;
  }

  getValorPorHoraEmCorrida(diaria: Diaria):number {
    let minutosTotais = this.calcularTotalMinutosEmCorrida(diaria);
    return (this.getValorRecebidoTotal(diaria) / ( minutosTotais / 60 ) )  ;
  }

  calcularTotalMinutosEmCorrida(diaria: Diaria):number {
    let minutosTotais:number = 0;
    diaria.rendimentosDiarios.map((rendimento) => {
      let arrAuxTempoEmCorrida = rendimento.tempoEmCorrida.split(":");
      let minutosEmCorrida = Number(arrAuxTempoEmCorrida[0]) * 60 + Number(arrAuxTempoEmCorrida[1]);
      minutosTotais += minutosEmCorrida;
    });
    return minutosTotais;
  }

  getValorPorDistancia(diaria: Diaria):number {
    return this.getValorRecebidoTotal(diaria) / this.getDistanciaTotal(diaria);
  }

}

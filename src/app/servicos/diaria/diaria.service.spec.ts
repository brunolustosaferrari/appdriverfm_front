import { TestBed } from '@angular/core/testing';

import { DiariaService } from './diaria.service';

describe('DiariaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DiariaService = TestBed.get(DiariaService);
    expect(service).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AutenticacaoModule } from './modulos/autenticacao/autenticacao/autenticacao.module';
import { UsuarioModule } from './modulos/usuario/usuario.module';
import { PaginaNaoEncontradaComponent } from './componentes/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { HomeComponent } from './componentes/home/home.component';
import { NavegacaoComponent } from './componentes/navegacao/navegacao.component';
import { HttpXsrfInterceptor } from './httpxsrf.interceptor';
import { VeiculoModule } from './modulos/veiculo/veiculo.module';
import { AplicativoModule } from './modulos/aplicativo/aplicativo.module';
import { DiariaModule } from './modulos/diaria/diaria.module';
import { CustoModule } from './modulos/custo/custo.module';
import { RelatoriosModule } from './modulos/relatorios/relatorios.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PaginaNaoEncontradaComponent,
    NavegacaoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AutenticacaoModule,
    HttpClientModule,
    FormsModule,
    UsuarioModule,
    VeiculoModule,
    AplicativoModule,
    DiariaModule,
    BrowserAnimationsModule,
    CustoModule,
    RelatoriosModule
  ],
  providers: [ { provide: HTTP_INTERCEPTORS, useClass: HttpXsrfInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
}

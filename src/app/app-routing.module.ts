import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';
import { LoginComponent } from './componentes/autenticacao/login/login.component';
import { CadastroUsuarioComponent } from './componentes/usuario/cadastro-usuario/cadastro-usuario.component';
import { PaginaNaoEncontradaComponent } from './componentes/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { HomeComponent } from './componentes/home/home.component';
import { AutenticacaoGuard } from './autenticacao.guard';
import { VeiculoModule } from './modulos/veiculo/veiculo.module';
import { AplicativoModule } from './modulos/aplicativo/aplicativo.module';
import { DiariaModule } from './modulos/diaria/diaria.module';
import { CustoModule } from './modulos/custo/custo.module';
import { RelatoriosModule } from './modulos/relatorios/relatorios.module';

const AppRoutes: Routes = [
  { path: "login",  component: LoginComponent },
  { path: "cadastro", component: CadastroUsuarioComponent},
  { path: "interna",
    component: HomeComponent,
    canActivate: [ AutenticacaoGuard ]
  },
  { path: "veiculos", loadChildren: () => VeiculoModule },
  { path: "aplicativos", loadChildren: () => AplicativoModule },
  { path: "diarias", loadChildren: () => DiariaModule },
  { path: "custos", loadChildren: () => CustoModule },
  { path: "relatorios", loadChildren: () => RelatoriosModule },
  { path: "", redirectTo: "interna", pathMatch: "full"},
  { path: "**", component: PaginaNaoEncontradaComponent }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(
      AppRoutes,
      { enableTracing: false}
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

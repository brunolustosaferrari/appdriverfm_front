import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginacao-custo',
  templateUrl: './paginacao-custo.component.html',
  styleUrls: ['./paginacao-custo.component.css']
})
export class PaginacaoCustoComponent implements OnInit {

  @Input() private contagemPaginas: number = 0;
  @Input() private paginaAtual: number = 0;
  private arrPaginas: number[] = new Array<number>();
  @Output() private eventoTrocar = new EventEmitter();

  constructor() { }

  ngOnInit() {
    for( let i:number = 0; i <= this.contagemPaginas; i++){
      this.arrPaginas.push(i);
    }
  }

  irParaPagina(pagina:number){
    this.paginaAtual = pagina;
    this.eventoTrocar.emit(pagina);
  }
}

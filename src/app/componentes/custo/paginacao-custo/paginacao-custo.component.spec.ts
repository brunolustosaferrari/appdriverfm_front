import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginacaoCustoComponent } from './paginacao-custo.component';

describe('PaginacaoCustoComponent', () => {
  let component: PaginacaoCustoComponent;
  let fixture: ComponentFixture<PaginacaoCustoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginacaoCustoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginacaoCustoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

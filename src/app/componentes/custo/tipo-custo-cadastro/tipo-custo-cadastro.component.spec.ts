import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCustoCadastroComponent } from './tipo-custo-cadastro.component';

describe('TipoCustoCadastroComponent', () => {
  let component: TipoCustoCadastroComponent;
  let fixture: ComponentFixture<TipoCustoCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCustoCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCustoCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

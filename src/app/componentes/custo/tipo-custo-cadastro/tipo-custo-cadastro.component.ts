import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TipoCusto } from 'src/app/classes/tipo-custo';
import { TipoCustoService } from 'src/app/servicos/custo/tipo-custo.service';

@Component({
  selector: 'app-tipo-custo-cadastro',
  templateUrl: './tipo-custo-cadastro.component.html',
  styleUrls: ['./tipo-custo-cadastro.component.css']
})
export class TipoCustoCadastroComponent implements OnInit {

  private tipo: TipoCusto;

  constructor(
    private servico: TipoCustoService,
    private dialogRef: MatDialogRef<TipoCustoCadastroComponent>) { }

  ngOnInit() {
    this.tipo = new TipoCusto();
  }

  salvar(){
    this.servico.inserir(this.tipo).subscribe((retorno:TipoCusto) => this.dialogRef.close(retorno));
  }

}

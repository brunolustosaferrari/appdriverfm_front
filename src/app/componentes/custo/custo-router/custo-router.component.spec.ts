import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustoRouterComponent } from './custo-router.component';

describe('CustoRouterComponent', () => {
  let component: CustoRouterComponent;
  let fixture: ComponentFixture<CustoRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustoRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustoRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

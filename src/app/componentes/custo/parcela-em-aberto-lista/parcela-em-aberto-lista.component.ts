import { Component, OnInit } from '@angular/core';
import { ParcelaService } from 'src/app/servicos/custo/parcela.service';
import { PageEvent } from '@angular/material/paginator';
import { Parcela } from 'src/app/classes/parcela';
import { Custo } from 'src/app/classes/custo';

@Component({
  selector: 'app-parcela-em-aberto-lista',
  templateUrl: './parcela-em-aberto-lista.component.html',
  styleUrls: ['./parcela-em-aberto-lista.component.css']
})
export class ParcelaEmAbertoListaComponent implements OnInit {

  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageIndex = 0;
  parcelas:Parcela[];

  constructor(private servico:ParcelaService) { }

  ngOnInit() {
    this.parcelas = new Array<Parcela>();
    this.contagemEmAberto();
    this.listar(0,10);
  }

  listarPagina(evento:PageEvent){
    this.listar(evento.pageIndex, evento.pageSize);
  }

  listar(pagina:number, tamanho:number){
    this.servico.listarEmAberto(pagina, tamanho).subscribe(
      (data) => {
        this.lerLista(data);
      }
    )
  }

  contagemEmAberto(){
    this.servico.buscarContagemParcelas().subscribe(
      (data) => {
      if(data.content != undefined){
        this.length = Number(data.content);
      }
    });
  }

  lerLista(data: Object): any {
    if(data._embedded != undefined){
      let arr = data._embedded.parcelaList;
      arr.map( ( parcela:Parcela ) => {
        let auxParcela = new Parcela();
        auxParcela.dataVencimento = new Date(parcela.dataVencimento);
        auxParcela.custo = parcela.custo;
        auxParcela.idParcela = parcela.idParcela;
        auxParcela.pago = parcela.pago;
        auxParcela.valor = parcela.valor;
        this.parcelas.push(auxParcela);
      });
    }
  }

  pagar(parcela:Parcela){
    parcela.pago = true;
    this.servico.atualizar(parcela).subscribe(
      () => {
        alert("Parcela paga");
        this.parcelas.splice(0 , this.parcelas.length);
        this.contagemEmAberto();
        this.listar(this.pageIndex, this.pageSize);
      }
    )
  }

}

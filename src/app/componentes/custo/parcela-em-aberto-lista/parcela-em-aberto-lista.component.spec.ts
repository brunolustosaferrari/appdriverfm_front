import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParcelaEmAbertoListaComponent } from './parcela-em-aberto-lista.component';

describe('ParcelaEmAbertoListaComponent', () => {
  let component: ParcelaEmAbertoListaComponent;
  let fixture: ComponentFixture<ParcelaEmAbertoListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParcelaEmAbertoListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParcelaEmAbertoListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

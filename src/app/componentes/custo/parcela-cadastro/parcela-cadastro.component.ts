import { Component, OnInit, Input } from '@angular/core';
import { Parcela } from 'src/app/classes/parcela';
import createNumberMask from 'text-mask-addons/dist/createNumberMask'

@Component({
  selector: 'app-parcela-cadastro',
  templateUrl: './parcela-cadastro.component.html',
  styleUrls: ['./parcela-cadastro.component.css']
})
export class ParcelaCadastroComponent implements OnInit {

  @Input() parcela: Parcela;

  private mascaraRs = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    requireDecimal: false

  });

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { Custo } from 'src/app/classes/custo';
import { CustoService } from 'src/app/servicos/custo/custo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-custo-painel',
  templateUrl: './custo-painel.component.html',
  styleUrls: ['./custo-painel.component.css']
})
export class CustoPainelComponent implements OnInit {

  private custos:Custo[];
  private contagemPaginas: number = 0;
  private paginaAtual: number = 0;

  constructor(private servico: CustoService, private router:Router) { }

  ngOnInit() {
    this.listarTodas();
  }

  listarTodas(pagina?: number){
    if(pagina != undefined){
      this.paginaAtual = pagina;
    }
    this.custos = new Array<Custo>();
    this.servico.listar(pagina).subscribe( (data) => {
      this.lerCustos(data);
    } );
    this.servico.buscarContagemPaginas().subscribe(
      (data) => {
        if(data.content != undefined){
          this.contagemPaginas = Number(data.content);
        }
      });

  }

  lerCustos(data:any){
    if(data._embedded != undefined){
      let arr: Custo[] = data._embedded.custoList;
      if(arr.length > 0){
        arr.map(
          (custo:Custo) => {
            let auxCusto = new Custo();
            auxCusto.idCusto = custo.idCusto;
            auxCusto.parcelas = custo.parcelas;
            auxCusto.data = new Date(custo.data);
            auxCusto.descricao = custo.descricao;
            auxCusto.titulo = custo.titulo;
            auxCusto.tipo = custo.tipo;
            auxCusto.veiculo = custo.veiculo;
            this.custos.push(auxCusto);
          }
        );
      }
    }
  }

  excluir(custo: Custo){
    this.servico.excluir(custo.idCusto).subscribe(
      () => {
        alert("Excluído");
        this.ngOnInit();
      }
    )
  }

}

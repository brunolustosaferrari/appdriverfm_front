import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustoPainelComponent } from './custo-painel.component';

describe('CustoPainelComponent', () => {
  let component: CustoPainelComponent;
  let fixture: ComponentFixture<CustoPainelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustoPainelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustoPainelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Custo } from 'src/app/classes/custo';
import { Veiculo } from 'src/app/interfaces/veiculo';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';
import { TipoCustoService } from 'src/app/servicos/custo/tipo-custo.service';
import { CustoService } from 'src/app/servicos/custo/custo.service';
import { TipoCusto } from 'src/app/classes/tipo-custo';
import { Parcela } from 'src/app/classes/parcela';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TipoCustoCadastroComponent } from '../tipo-custo-cadastro/tipo-custo-cadastro.component';

@Component({
  selector: 'app-custo-cadastro',
  templateUrl: './custo-cadastro.component.html',
  styleUrls: ['./custo-cadastro.component.css']
})
export class CustoCadastroComponent implements OnInit {

  private custo: Custo;
  private veiculos: Veiculo[];
  private tipos: TipoCusto[];

  private tituloOperacao:string;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private servico: CustoService,
    private servicoVeiculo: VeiculoService,
    private servicoTipo: TipoCustoService) { }

  ngOnInit() {
    this.custo = new Custo();
    if(this.route.snapshot.params['id'] == null){
      this.tituloOperacao="Cadastrar";
      this.custo.parcelas.push(new Parcela());
    }else{
      this.tituloOperacao="Atualizar";
      this.servico.buscar(this.route.snapshot.params['id']).subscribe(
        (custo:Custo)=>{
          delete custo["_links"];
          let auxCusto = new Custo();
          auxCusto.idCusto = custo.idCusto;
          custo.parcelas.map( (parcela:Parcela) => {
            parcela.dataVencimento = new Date(parcela.dataVencimento);
            auxCusto.parcelas.push(parcela);
          });
          auxCusto.data = new Date(custo.data);
          auxCusto.descricao = custo.descricao;
          auxCusto.titulo = custo.titulo;
          auxCusto.veiculo = custo.veiculo;
          auxCusto.tipo = custo.tipo;
          this.custo = auxCusto;
        }
      )
    }
    this.buscarOpcoesVeiculo();
    this.buscarOpcoesTipo();
  }

  buscarOpcoesVeiculo() {
    this.servicoVeiculo.listar().subscribe(
      (data) => {
        if(data._embedded != undefined){
          let arr: Veiculo[] = data._embedded.veiculoList;
          if(arr.length > 0){
            this.veiculos = new Array<Veiculo>();
            arr.map(
              (veiculo:Veiculo) => {
                delete veiculo["_links"];
                this.veiculos.push(veiculo);
              }
            );
          }
        }
      });
  }

  buscarOpcoesTipo() {
    this.servicoTipo.listar().subscribe(
      (data) => {
        if(data._embedded != undefined){
          let arr: TipoCusto[] = data._embedded.tipoCustoList;
          if(arr.length > 0){
            this.tipos = new Array<TipoCusto>();
            arr.map(
              (tipo:TipoCusto) => {
                delete tipo["_links"];
                this.tipos.push(tipo);
              }
            );
          }
        }
      });
  }

  compararVeiculo(opcao1:Veiculo, opcao2:Veiculo){
    return opcao1.idVeiculo == opcao2.idVeiculo;
  }

  compararTipo(opcao1:TipoCusto, opcao2:TipoCusto){
    return opcao1.idTipoCusto == opcao2.idTipoCusto;
  }

  adicionarParcela(){
    this.custo.parcelas.push(new Parcela());
  }

  adicionarTipo(){
    let dialogRef = this.dialog.open(TipoCustoCadastroComponent);
    dialogRef.afterClosed().subscribe( (result:TipoCusto) => {
      if(result.descricao != undefined){
        this.tipos.push(result);
      }
    });
  }

  removerParcela(indice: number){
    this.custo.parcelas.splice(indice,1);
  }

  salvar(){
    let metodo = "inserir";
    if(this.custo.idCusto != undefined){
      metodo="atualizar";
    }
    this.servico[metodo](this.custo).subscribe(
      () =>{
        alert("Cadastrado com sucesso");
        this.router.navigateByUrl("/custos");
      },
      (erro:string) => alert("Ocorreu um erro: " + erro + ".Tente novamente")
    );


  }

}

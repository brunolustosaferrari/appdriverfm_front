import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustoCadastroComponent } from './custo-cadastro.component';

describe('CustoCadastroComponent', () => {
  let component: CustoCadastroComponent;
  let fixture: ComponentFixture<CustoCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustoCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustoCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

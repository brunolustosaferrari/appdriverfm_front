import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustoDetalhesComponent } from './custo-detalhes.component';

describe('CustoDetalhesComponent', () => {
  let component: CustoDetalhesComponent;
  let fixture: ComponentFixture<CustoDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustoDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustoDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

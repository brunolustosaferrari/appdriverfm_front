import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Veiculo } from 'src/app/interfaces/veiculo';

@Component({
  selector: 'app-veiculo-detalhes',
  templateUrl: './veiculo-detalhes.component.html',
  styleUrls: ['./veiculo-detalhes.component.css']
})
export class VeiculoDetalhesComponent implements OnInit {

  @Input() veiculo: Veiculo;
  @Output() eventoExcluir = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
}

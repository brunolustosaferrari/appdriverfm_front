import { Component, OnInit } from '@angular/core';
import { Veiculo } from 'src/app/interfaces/veiculo';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';

@Component({
  selector: 'app-veiculo-painel',
  templateUrl: './veiculo-painel.component.html',
  styleUrls: ['./veiculo-painel.component.css']
})
export class VeiculoPainelComponent implements OnInit {

  private veiculos: Veiculo[];

  constructor(private servico: VeiculoService) { }

  ngOnInit() {
    this.servico.listar().subscribe(
      (data) => {
        this.veiculos = data._embedded.veiculoList;
      });

  }

  excluir(veiculo:Veiculo){
    this.servico.excluir(veiculo).subscribe(
      data => {
        alert("Veiculo excluido");
        this.ngOnInit();
      }
    );
  }

}

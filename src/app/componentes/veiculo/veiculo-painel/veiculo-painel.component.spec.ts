import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeiculoPainelComponent } from './veiculo-painel.component';

describe('VeiculoPainelComponent', () => {
  let component: VeiculoPainelComponent;
  let fixture: ComponentFixture<VeiculoPainelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculoPainelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculoPainelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

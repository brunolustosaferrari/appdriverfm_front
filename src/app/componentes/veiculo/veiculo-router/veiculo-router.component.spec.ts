import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeiculoRouterComponent } from './veiculo-router.component';

describe('VeiculoRouterComponent', () => {
  let component: VeiculoRouterComponent;
  let fixture: ComponentFixture<VeiculoRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculoRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculoRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

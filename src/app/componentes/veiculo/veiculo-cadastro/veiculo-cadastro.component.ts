import { Component, OnInit, Input } from '@angular/core';
import { Veiculo } from 'src/app/interfaces/veiculo';
import {ActivatedRoute} from '@angular/router';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';

@Component({
  selector: 'app-veiculo-cadastro',
  templateUrl: './veiculo-cadastro.component.html',
  styleUrls: ['./veiculo-cadastro.component.css']
})
export class VeiculoCadastroComponent implements OnInit {

  @Input() veiculo: Veiculo;

  constructor(private route: ActivatedRoute, private servico: VeiculoService) { }

  ngOnInit() {
    if ( this.route.snapshot.params['id'] != null ) {
      this.servico.buscar(this.route.snapshot.params['id']).subscribe((data:Veiculo)=> this.veiculo = data);
    }else{
      this.veiculo = {} as Veiculo;
    }
  }

  salvar(){
    if ( this.route.snapshot.params['id'] == null ) {
      this.servico.cadastrar(this.veiculo).subscribe(
        () => {
          alert("Cadastrado com sucesso");
          window.location.href = '/veiculos';
        },
        ( erro: Error ) =>{
          alert("Ocorreu um erro:" + erro.message + ". Tente novamente.");
        }
      );
    }else{
      this.servico.atualizar(this.veiculo).subscribe(
        () => {
          alert("Veículo alterado com sucesso");
            window.location.href = '/veiculos';
        },
        ( erro: Error ) =>{
          alert("Ocorreu um erro:" + erro.message + ". Tente novamente.");
        }
      )
    }
  }

}

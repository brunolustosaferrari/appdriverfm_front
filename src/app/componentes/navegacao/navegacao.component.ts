import { Component, OnInit } from '@angular/core';
import { AutenticacaoService } from 'src/app/servicos/autenticacao/autenticacao.service';
import { CookieService } from 'angular2-cookie';

@Component({
  selector: 'app-navegacao',
  templateUrl: './navegacao.component.html',
  styleUrls: ['./navegacao.component.css']
})
export class NavegacaoComponent implements OnInit {

  constructor(private servicoAutenticacao: AutenticacaoService, private cookieServico: CookieService) { }

  ngOnInit() {
  }

  checkAutenticado(): boolean  {
    return this.servicoAutenticacao.checkCookieAutenticacao();
  }

  sair(){
    this.cookieServico.remove("tokenAutenticacao");
    window.location.href = "";
  }
}

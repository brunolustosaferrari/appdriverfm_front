import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplicativoDetalhesComponent } from './aplicativo-detalhes.component';

describe('AplicativoDetalhesComponent', () => {
  let component: AplicativoDetalhesComponent;
  let fixture: ComponentFixture<AplicativoDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicativoDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplicativoDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

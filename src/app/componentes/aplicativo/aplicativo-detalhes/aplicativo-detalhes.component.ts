import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Aplicativo } from 'src/app/interfaces/aplicativo';

@Component({
  selector: 'app-aplicativo-detalhes',
  templateUrl: './aplicativo-detalhes.component.html',
  styleUrls: ['./aplicativo-detalhes.component.css']
})
export class AplicativoDetalhesComponent implements OnInit {

  @Output() eventoExcluir = new EventEmitter();
  @Input() private aplicativo: Aplicativo;

  constructor() { }

  ngOnInit() {
  }



}

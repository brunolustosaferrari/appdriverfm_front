import { Component, OnInit } from '@angular/core';
import { AplicativoService } from 'src/app/servicos/aplicativo/aplicativo.service';
import { Aplicativo } from 'src/app/interfaces/aplicativo';

@Component({
  selector: 'app-aplicativo-painel',
  templateUrl: './aplicativo-painel.component.html',
  styleUrls: ['./aplicativo-painel.component.css']
})
export class AplicativoPainelComponent implements OnInit {

  private aplicativos: Aplicativo[];

  constructor(private servico:AplicativoService) { }

  ngOnInit() {
    this.aplicativos = {} as Aplicativo[];
    this.servico.listar()
      .subscribe(data => {
        let arr = data._embedded.aplicativoList;
        if( arr.length > 0){
          this.aplicativos = arr;
          console.log(this.aplicativos);
        }
      });

  }

  excluir(aplicativo: Aplicativo){
    this.servico.excluir(aplicativo.idAplicativo)
      .subscribe(()=>{
        alert("Aplicativo excluído com sucesso");
        this.ngOnInit();
      })
  }

}

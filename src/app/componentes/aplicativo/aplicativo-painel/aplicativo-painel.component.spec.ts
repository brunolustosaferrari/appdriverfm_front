import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplicativoPainelComponent } from './aplicativo-painel.component';

describe('AplicativoPainelComponent', () => {
  let component: AplicativoPainelComponent;
  let fixture: ComponentFixture<AplicativoPainelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicativoPainelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplicativoPainelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

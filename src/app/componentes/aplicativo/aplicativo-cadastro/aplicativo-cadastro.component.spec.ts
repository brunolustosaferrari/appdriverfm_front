import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplicativoCadastroComponent } from './aplicativo-cadastro.component';

describe('AplicativoCadastroComponent', () => {
  let component: AplicativoCadastroComponent;
  let fixture: ComponentFixture<AplicativoCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicativoCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplicativoCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

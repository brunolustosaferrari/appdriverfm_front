import { Component, OnInit, Input } from '@angular/core';
import { AplicativoService } from 'src/app/servicos/aplicativo/aplicativo.service';
import { Aplicativo } from 'src/app/interfaces/aplicativo';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-aplicativo-cadastro',
  templateUrl: './aplicativo-cadastro.component.html',
  styleUrls: ['./aplicativo-cadastro.component.css']
})
export class AplicativoCadastroComponent implements OnInit {

  @Input() aplicativo: Aplicativo;

  constructor(private servico: AplicativoService, private router: ActivatedRoute) { }

  ngOnInit() {
    if(this.router.snapshot.params['id'] != null){
      this.servico.buscar(this.router.snapshot.params['id'])
          .subscribe(data => this.aplicativo = data);
    }else{
      this.aplicativo = {} as Aplicativo;
    }
  }

  salvar(){
    let metodo = "inserir";
    if(this.aplicativo.idAplicativo != null){
      metodo = "atualizar";
    }
    this.servico[metodo](this.aplicativo).subscribe(
      ()=>{
        alert("Aplicativo cadastrado com sucesso");
        window.location.href = "/aplicativos";
      },
      (erro: Error) =>{
        alert("Erro ao cadastrar. " + erro.message + " Tente novamente.")
      }
    )
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplicativoRouterComponent } from './aplicativo-router.component';

describe('AplicativoRouterComponent', () => {
  let component: AplicativoRouterComponent;
  let fixture: ComponentFixture<AplicativoRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicativoRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplicativoRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

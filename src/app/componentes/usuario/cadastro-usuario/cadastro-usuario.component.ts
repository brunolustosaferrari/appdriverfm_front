import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from 'src/app/servicos/usuario/usuario.service';
import { Usuario } from 'src/app/interfaces/usuario';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.css']
})

export class CadastroUsuarioComponent implements OnInit {

  @Input() usuario: Usuario;
  confirmacaoSenha: String;

  constructor(private servico: UsuarioService) { }

  ngOnInit() {
    this.usuario = {} as Usuario;
  }

  salvar(){
    if(this.usuario.senha == this.confirmacaoSenha){
      this.servico.cadastrar(this.usuario).subscribe(
        () => {
          alert("Usuario cadastrado com sucesso, efetue seu login.");
          location.href = "/login";
        },
        error => {
          alert(error);
        }
      );
    }else{
      alert("A senha não confere");
    }
  }
}

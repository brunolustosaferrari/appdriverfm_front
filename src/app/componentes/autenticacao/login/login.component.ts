import { Component, OnInit} from '@angular/core';
import { Usuario } from 'src/app/interfaces/usuario';
import { AutenticacaoService } from 'src/app/servicos/autenticacao/autenticacao.service';
import { CookieService, CookieOptionsArgs } from 'angular2-cookie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  usuario: Usuario;

  constructor(
    private servico: AutenticacaoService,
    private cookieServico: CookieService
  ) { }

  ngOnInit() {
    this.usuario = {} as Usuario;
  }

  autenticar(){
    this.servico.autenticar(this.usuario).subscribe(
      (data: string) => {
        let opcoesCookie = {} as CookieOptionsArgs;
        let hoje = new Date();
        hoje.setDate(hoje.getDate() + 7);
        opcoesCookie.expires = hoje;
        opcoesCookie.secure = false;
        this.cookieServico.put("tokenAutenticacao", data, opcoesCookie);
        window.location.href = "/";

      },
      error => alert(error)
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiariaRouterComponent } from './diaria-router.component';

describe('DiariaRouterComponent', () => {
  let component: DiariaRouterComponent;
  let fixture: ComponentFixture<DiariaRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiariaRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiariaRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Diaria } from 'src/app/classes/diaria';
import { DiariaService } from 'src/app/servicos/diaria/diaria.service';
import { RendimentoDiarioService } from 'src/app/servicos/rendimento-diario/rendimento-diario.service';

@Component({
  selector: 'app-diaria-painel',
  templateUrl: './diaria-painel.component.html',
  styleUrls: ['./diaria-painel.component.css']
})
export class DiariaPainelComponent implements OnInit {

  private diarias:Diaria[];
  private contagemPaginas: number = 0;
  private paginaAtual: number = 0;

  constructor(private servico: DiariaService, private servicoRendimento: RendimentoDiarioService) { }

  ngOnInit() {
    this.listarSemana();
  }

  excluir(diaria: Diaria){
    this.servico.excluir(diaria.idDiaria).subscribe(
      () => {
        alert("Diária excluída");
        this.ngOnInit();
      },
      () =>{
        alert("Ocorreu um erro ao excluir");
      }
    )
  }

  listarSemana(){
    this.diarias = new Array<Diaria>();
    this.servico.listarSemana().subscribe( (data) => {
      this.lerDiarias(data);
    } );
    this.contagemPaginas = 0
  }

  listarMes(){
    this.diarias = new Array<Diaria>();
    this.servico.listarMes().subscribe( (data) => {
      this.lerDiarias(data);
    } );
    this.contagemPaginas = 0
  }

  listarTodas(pagina?: number){
    if(pagina != undefined){
      this.paginaAtual = pagina;
    }
    this.diarias = new Array<Diaria>();
    this.servico.listar(pagina).subscribe( (data) => {
      this.lerDiarias(data);
    } );
    this.servico.buscarContagemPaginas().subscribe(
      (data) => {
        if(data.content != undefined){
          this.contagemPaginas = Number(data.content);
        }
      });

  }

  lerDiarias(data:any){
    if(data._embedded != undefined){
      let arr: Diaria[] = data._embedded.diariaList;
      if(arr.length > 0){
        arr.map(
          (diaria:Diaria) => {
            let auxDiaria = new Diaria();
            auxDiaria.idDiaria = diaria.idDiaria;
            auxDiaria.rendimentosDiarios = diaria.rendimentosDiarios;
            auxDiaria.dataHoraInicio = new Date(diaria.dataHoraInicio);
            auxDiaria.dataHoraFim = new Date(diaria.dataHoraFim);
            this.diarias.push(auxDiaria);
          }
        );
      }
    }
  }
}

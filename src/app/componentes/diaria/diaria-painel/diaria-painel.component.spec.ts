import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiariaPainelComponent } from './diaria-painel.component';

describe('DiariaPainelComponent', () => {
  let component: DiariaPainelComponent;
  let fixture: ComponentFixture<DiariaPainelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiariaPainelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiariaPainelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

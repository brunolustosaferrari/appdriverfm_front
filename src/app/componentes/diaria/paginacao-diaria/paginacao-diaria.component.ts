import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-paginacao-diaria',
  templateUrl: './paginacao-diaria.component.html',
  styleUrls: ['./paginacao-diaria.component.css']
})
export class PaginacaoDiariaComponent implements OnInit {

  @Input() private contagemPaginas: number = 0;
  @Input() private paginaAtual: number = 0;
  private arrPaginas: number[] = new Array<number>();
  @Output() private eventoTrocar = new EventEmitter();

  constructor() { }

  ngOnInit() {
    for( let i:number = 0; i <= this.contagemPaginas; i++){
      this.arrPaginas.push(i);
    }
  }

  irParaPagina(pagina:number){
    this.paginaAtual = pagina;
    this.eventoTrocar.emit(pagina);
  }

}

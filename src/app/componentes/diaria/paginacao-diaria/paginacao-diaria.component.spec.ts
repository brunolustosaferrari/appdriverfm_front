import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginacaoDiariaComponent } from './paginacao-diaria.component';

describe('PaginacaoDiariaComponent', () => {
  let component: PaginacaoDiariaComponent;
  let fixture: ComponentFixture<PaginacaoDiariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginacaoDiariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginacaoDiariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

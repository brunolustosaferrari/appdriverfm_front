import { Component, OnInit, Input } from '@angular/core';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';
import { Veiculo } from 'src/app/interfaces/veiculo';
import { AplicativoService } from 'src/app/servicos/aplicativo/aplicativo.service';
import { Aplicativo } from 'src/app/interfaces/aplicativo';

import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import { RendimentoDiario } from 'src/app/classes/rendimento-diario';

@Component({
  selector: 'app-rendimento-diario-detalhes',
  templateUrl: './rendimento-diario-detalhes.component.html',
  styleUrls: ['./rendimento-diario-detalhes.component.css']
})
export class RendimentoDiarioDetalhesComponent implements OnInit {

  @Input() private rendimento: RendimentoDiario;

  private veiculos: Veiculo[];
  private aplicativos: Aplicativo[];
  private tempoEmCorridaInput: string = "";

  private mascaraRs = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 2,
    requireDecimal: false

  });

  private mascaraKm = createNumberMask({
    prefix: "",
    suffix: "",
    includeThousandsSeparator: false,
    allowDecimal: true,
    decimalSymbol: ".",
    decimalLimit: 3,
    requireDecimal: false
  });

  constructor(private servicoVeiculo: VeiculoService,
     private servicoAplicativo: AplicativoService) {

  }

  ngOnInit() {
    this.buscarOpcoesVeiculo();
    this.buscarOpcoesAplicativo();
  }

  buscarOpcoesVeiculo() {
    this.servicoVeiculo.listar().subscribe(
      (data) => {
        if(data._embedded != undefined){
          let arr: Veiculo[] = data._embedded.veiculoList;
          if(arr.length > 0){
            this.veiculos = new Array<Veiculo>();
            arr.map(
              (veiculo:Veiculo) => {
                delete veiculo["_links"];
                this.veiculos.push(veiculo);
              }
            );
          }
        }
      });
  }

  buscarOpcoesAplicativo(){
    this.servicoAplicativo.listar()
      .subscribe(data => {
        if(data._embedded != undefined){
          let arr:Aplicativo[] = data._embedded.aplicativoList;
          if( arr.length > 0){
            this.aplicativos = new Array<Aplicativo>();
            arr.map(
              (aplicativo:Aplicativo) => {
                delete aplicativo["_links"];
                this.aplicativos.push(aplicativo);
              }
            );
          }
        }
      });
  }

  atualizarTempoEmCorrida(){
    let arrTempoInput = this.tempoEmCorridaInput.split(":");
    let dateTempoInput = new Date(0);
    dateTempoInput.setUTCHours(Number(arrTempoInput[0]));
    dateTempoInput.setUTCMinutes(Number(arrTempoInput[1]));
    let strTempoInputISOTime:string = dateTempoInput.toISOString();
    this.rendimento.tempoEmCorrida = strTempoInputISOTime;
  }

  compararAplicativo(opcao1:any, opcao2:any){
    return opcao1.idAplicativo == opcao2.idAplicativo;
  }

  compararVeiculo(opcao1:any, opcao2:any){
    return opcao1.idVeiculo == opcao2.idVeiculo;
  }

}

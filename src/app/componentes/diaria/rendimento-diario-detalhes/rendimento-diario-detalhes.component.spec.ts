import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendimentoDiarioDetalhesComponent } from './rendimento-diario-detalhes.component';

describe('RendimentoDiarioDetalhesComponent', () => {
  let component: RendimentoDiarioDetalhesComponent;
  let fixture: ComponentFixture<RendimentoDiarioDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendimentoDiarioDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendimentoDiarioDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DiariaService } from 'src/app/servicos/diaria/diaria.service';
import { RendimentoDiario } from 'src/app/classes/rendimento-diario';
import { Diaria } from 'src/app/classes/diaria';

@Component({
  selector: 'app-diaria-cadastro',
  templateUrl: './diaria-cadastro.component.html',
  styleUrls: ['./diaria-cadastro.component.css']
})
export class DiariaCadastroComponent implements OnInit {

  private diaria:Diaria;
  private dataInicio:Date;
  private dataFim:Date;
  private horaInicio:string ;
  private horaFim:string;

  private tituloOperacao:string;

  constructor(private route: ActivatedRoute, private servico:DiariaService, private router:Router) { }

  ngOnInit() {
    this.diaria = new Diaria();
    if(this.route.snapshot.params['id'] == null){
      this.tituloOperacao = "Nova";
      this.diaria.dataHoraFim = new Date();
      this.diaria.dataHoraInicio = new Date();
      this.horaInicio = "";
      this.horaFim = "";
      this.diaria.rendimentosDiarios = new Array<RendimentoDiario>();
      this.diaria.rendimentosDiarios.push(new RendimentoDiario());
    }else{
      this.tituloOperacao = "Alterar";
      this.servico.buscar(this.route.snapshot.params['id'])
      .subscribe((data:Diaria) => {
        delete data["_links"];
        this.diaria = data;
        this.diaria.rendimentosDiarios = data.rendimentosDiarios;
        this.diaria.dataHoraFim = new Date(data.dataHoraFim);
        this.diaria.dataHoraInicio = new Date(data.dataHoraInicio);
        this.horaInicio = this.diaria.dataHoraInicio.toTimeString().substring(0,5);
        this.horaFim = this.diaria.dataHoraFim.toTimeString().substring(0,5);
        this.dataInicio = new Date(data.dataHoraInicio);
        this.dataFim = new Date(data.dataHoraFim);
      });
    }
  }

  atualizarHoraInicio(){
    let arrhoraInicio:Array<string> = this.horaInicio.split(":");
    this.diaria.dataHoraInicio.setHours(Number(arrhoraInicio[0]));
    this.diaria.dataHoraInicio.setMinutes(Number(arrhoraInicio[1]));
  }

  atualizarHoraFim(){
    let arrhoraFim:Array<string> = this.horaFim.split(":");
    this.diaria.dataHoraFim.setHours(Number(arrhoraFim[0]));
    this.diaria.dataHoraFim.setMinutes(Number(arrhoraFim[1]));
  }

  atualizarDataInicio(){
    this.diaria.dataHoraInicio.setTime(this.dataInicio.getTime());
  }

  atualizarDataFim(){
    this.diaria.dataHoraFim.setTime(this.dataFim.getTime());
  }

  adicionarRendimento(){
    this.diaria.rendimentosDiarios.push(new RendimentoDiario());
  }

  salvar(){
    let metodo = "inserir";
    if(this.diaria.idDiaria != null){
      metodo = "atualizar";
    }
    this.servico[metodo](this.diaria).subscribe(
      ()=>{
        alert("Diaria cadastrada com sucesso");
        this.router.navigateByUrl("/diarias");
      },
      (erro: string) =>{
        alert("Erro ao cadastrar. " + erro + " Tente novamente.")
      }
    )
  }

  removerRendimento(index: number){
    this.diaria.rendimentosDiarios.splice(index, 1);
  }
}

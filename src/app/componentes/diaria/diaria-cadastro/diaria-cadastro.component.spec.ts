import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiariaCadastroComponent } from './diaria-cadastro.component';

describe('DiariaCadastroComponent', () => {
  let component: DiariaCadastroComponent;
  let fixture: ComponentFixture<DiariaCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiariaCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiariaCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatoriosRouterComponent } from './relatorios-router.component';

describe('RelatoriosRouterComponent', () => {
  let component: RelatoriosRouterComponent;
  let fixture: ComponentFixture<RelatoriosRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatoriosRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatoriosRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FaturamentoService } from 'src/app/servicos/relatorios/faturamento.service';
import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { Faturamento } from 'src/app/classes/faturamento';

@Component({
  selector: 'app-faturamento',
  templateUrl: './faturamento.component.html',
  styleUrls: ['./faturamento.component.css']
})
export class FaturamentoComponent implements OnInit {

  public barChartOptions: ChartOptions = {
   responsive: true,
   // We use these empty structures as placeholders for dynamic theming.
   scales: { xAxes: [{}], yAxes: [{}] },
   plugins: {
     datalabels: {
       anchor: 'end',
       align: 'end',
     }
   }
 };
 public barChartLabels: Label[];
 public barChartType: ChartType = 'bar';
 public barChartLegend = true;
 public barChartData: ChartDataSets[];

 public porKmChartData: ChartDataSets[];

  constructor(private servico: FaturamentoService) { }

  ngOnInit() {
    this.servico.buscarPorAno("2019").subscribe(
      (data) => {
        this.barChartLabels = [];
        this.barChartData = new Array<ChartDataSets>();
        this.porKmChartData = new Array<ChartDataSets>();
        let arrTotais = new Array();
        Object.entries(data).map((arrDados, index) =>{
          let periodo: string = arrDados[0];
          let arrFaturamentos: Faturamento[] = arrDados[1];
          let total:number = 0;
          this.barChartLabels[index] = periodo.toString();

          arrFaturamentos.forEach((faturamento:Faturamento) => {
            let indiceAplicativo = this.barChartData.find((element) => this.encontrarIndiceAplicativo(element, faturamento.aplicativo));
            if( indiceAplicativo == undefined ){
              let aplicativoDataSet = {} as ChartDataSets;
              aplicativoDataSet.label = faturamento.aplicativo.nome;
              aplicativoDataSet.data = new Array<number>();
              aplicativoDataSet.data[index] = faturamento.valor;
              this.barChartData.push(aplicativoDataSet);
            }else{
              indiceAplicativo.data[index] = faturamento.valor;
            }
            total += faturamento.valor;

            let indiceAplicativoPorKm = this.porKmChartData.find((element) => this.encontrarIndiceAplicativo(element, faturamento.aplicativo));
            if( indiceAplicativoPorKm == undefined ){
              let aplicativoDataSet = {} as ChartDataSets;
              aplicativoDataSet.label = faturamento.aplicativo.nome;
              aplicativoDataSet.data = new Array<number>();
              aplicativoDataSet.data[index] = faturamento.valor / faturamento.distanciaPercorrida;
              this.porKmChartData.push(aplicativoDataSet);
            }else{
              indiceAplicativoPorKm.data[index] = faturamento.valor / faturamento.distanciaPercorrida;
            }

          });
          arrTotais[index] = total;
        });

        let totaisChartDataSet = {} as ChartDataSets;
        totaisChartDataSet.data = arrTotais;
        totaisChartDataSet.label = "Total";
        this.barChartData.push(totaisChartDataSet);
      });

  }
  encontrarIndiceAplicativo(element, aplicativo){
    if(element.label == aplicativo.nome){
      return true;
    }
    return false;
  }

}

import { Injectable } from '@angular/core';
import { HttpInterceptor,  HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'angular2-cookie';

@Injectable()
export class HttpXsrfInterceptor implements HttpInterceptor {

  constructor(private servicoCookie: CookieService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headerName = 'Authorization';
    let token = this.servicoCookie.get("tokenAutenticacao");
    if (token !== undefined && !req.headers.has(headerName)) {
      req = req.clone({ headers: req.headers.set(headerName, token) });
    }
    return next.handle(req);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from 'src/app/componentes/autenticacao/login/login.component';
import { AutenticacaoService } from 'src/app/servicos/autenticacao/autenticacao.service';
import { CookieService } from 'angular2-cookie/core';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[
    LoginComponent
  ],
  providers:[
    AutenticacaoService,
    CookieService
  ]
})
export class AutenticacaoModule { }

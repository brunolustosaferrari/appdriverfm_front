import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelatoriosRouterComponent } from 'src/app/componentes/relatorios/relatorios-router/relatorios-router.component';
import { AutenticacaoGuard } from 'src/app/autenticacao.guard';
import { FaturamentoComponent } from 'src/app/componentes/relatorios/faturamento/faturamento.component';

const routes: Routes = [
  {path: "",
   component: RelatoriosRouterComponent,
   canActivate: [AutenticacaoGuard],
   canActivateChild: [AutenticacaoGuard],
   children:[
     { path: "", component: FaturamentoComponent},
     { path: "faturamento", component: FaturamentoComponent}

   ]
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatoriosRoutingModule { }

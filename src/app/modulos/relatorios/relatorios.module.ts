import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { FaturamentoComponent } from 'src/app/componentes/relatorios/faturamento/faturamento.component';
import { FaturamentoService } from 'src/app/servicos/relatorios/faturamento.service';
import { RelatoriosRouterComponent } from 'src/app/componentes/relatorios/relatorios-router/relatorios-router.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    RelatoriosRouterComponent,
    FaturamentoComponent
  ],
  imports: [
    CommonModule,
    RelatoriosRoutingModule,
    ChartsModule
  ],
  providers: [
    FaturamentoService
  ]
})
export class RelatoriosModule { }

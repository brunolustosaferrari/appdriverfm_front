import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VeiculoRoutingModule } from './veiculo-routing.module';
import { VeiculoCadastroComponent } from 'src/app/componentes/veiculo/veiculo-cadastro/veiculo-cadastro.component';
import { VeiculoPainelComponent } from 'src/app/componentes/veiculo/veiculo-painel/veiculo-painel.component';
import { VeiculoRouterComponent } from 'src/app/componentes/veiculo/veiculo-router/veiculo-router.component';
import { FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';
import { VeiculoDetalhesComponent } from 'src/app/componentes/veiculo/veiculo-detalhes/veiculo-detalhes.component';


@NgModule({
  declarations: [
    VeiculoCadastroComponent,
    VeiculoPainelComponent,
    VeiculoRouterComponent,
    VeiculoDetalhesComponent
  ],
  imports: [
    CommonModule,
    VeiculoRoutingModule,
    FormsModule,
    TabsModule.forRoot()
  ],
  providers:[
    VeiculoService
  ]
})
export class VeiculoModule { }

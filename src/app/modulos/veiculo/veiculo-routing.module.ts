import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VeiculoPainelComponent } from 'src/app/componentes/veiculo/veiculo-painel/veiculo-painel.component';
import { VeiculoCadastroComponent } from 'src/app/componentes/veiculo/veiculo-cadastro/veiculo-cadastro.component';
import { AutenticacaoGuard } from 'src/app/autenticacao.guard';
import { VeiculoRouterComponent } from 'src/app/componentes/veiculo/veiculo-router/veiculo-router.component';

const routes: Routes = [{
  path: "",
  component: VeiculoRouterComponent,
  canActivate: [AutenticacaoGuard],
  canActivateChild: [AutenticacaoGuard],
  children:[{
      path: "cadastrar",
      component: VeiculoCadastroComponent
    },
    {
      path: "atualizar/:id",
      component: VeiculoCadastroComponent
    },
    {
      path: "",
      component: VeiculoPainelComponent,
      pathMatch: "full"
    }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeiculoRoutingModule { }

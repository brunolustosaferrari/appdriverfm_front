import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { CadastroUsuarioComponent } from 'src/app/componentes/usuario/cadastro-usuario/cadastro-usuario.component';
import { UsuarioService } from 'src/app/servicos/usuario/usuario.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CadastroUsuarioComponent
  ],
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    FormsModule
  ],
  exports: [
    CadastroUsuarioComponent
  ],
  providers: [
    UsuarioService
  ]
})
export class UsuarioModule { }

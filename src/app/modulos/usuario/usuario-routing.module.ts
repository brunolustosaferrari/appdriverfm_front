import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const UsuarioRoutes: Routes = []

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(
      UsuarioRoutes
    ),
    CommonModule
  ],
  exports:[
    RouterModule
  ]
})
export class UsuarioRoutingModule { }

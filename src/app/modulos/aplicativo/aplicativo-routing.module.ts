import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AplicativoRouterComponent } from 'src/app/componentes/aplicativo/aplicativo-router/aplicativo-router.component';
import { AutenticacaoGuard } from 'src/app/autenticacao.guard';
import { AplicativoPainelComponent } from 'src/app/componentes/aplicativo/aplicativo-painel/aplicativo-painel.component';
import { AplicativoCadastroComponent } from 'src/app/componentes/aplicativo/aplicativo-cadastro/aplicativo-cadastro.component';

const routes: Routes = [
  { path: "" ,
    component: AplicativoRouterComponent,
    canActivate: [AutenticacaoGuard],
    canActivateChild: [AutenticacaoGuard],
    children:[
      { path : "", component: AplicativoPainelComponent },
      { path: "cadastrar", component: AplicativoCadastroComponent },
      { path: "atualizar/:id", component: AplicativoCadastroComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AplicativoRoutingModule { }

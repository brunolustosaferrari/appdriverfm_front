import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AplicativoRoutingModule } from './aplicativo-routing.module';
import { AplicativoPainelComponent } from 'src/app/componentes/aplicativo/aplicativo-painel/aplicativo-painel.component';
import { AplicativoCadastroComponent } from 'src/app/componentes/aplicativo/aplicativo-cadastro/aplicativo-cadastro.component';
import { AplicativoRouterComponent } from 'src/app/componentes/aplicativo/aplicativo-router/aplicativo-router.component';
import { AplicativoService } from 'src/app/servicos/aplicativo/aplicativo.service';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AplicativoDetalhesComponent } from 'src/app/componentes/aplicativo/aplicativo-detalhes/aplicativo-detalhes.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AplicativoPainelComponent,
    AplicativoCadastroComponent,
    AplicativoRouterComponent,
    AplicativoDetalhesComponent
  ],
  imports: [
    CommonModule,
    AplicativoRoutingModule,
    TabsModule,
    FormsModule
  ],
  providers: [
    AplicativoService
  ]
})
export class AplicativoModule { }

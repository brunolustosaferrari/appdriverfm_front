import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTabsModule} from '@angular/material/tabs';
import {MatRadioModule} from '@angular/material/radio';

import { DiariaRoutingModule } from './diaria-routing.module';
import { DiariaRouterComponent } from 'src/app/componentes/diaria/diaria-router/diaria-router.component';
import { DiariaPainelComponent } from 'src/app/componentes/diaria/diaria-painel/diaria-painel.component';
import { DiariaCadastroComponent } from 'src/app/componentes/diaria/diaria-cadastro/diaria-cadastro.component';
import { MAT_DATE_LOCALE, MatNativeDateModule, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { RendimentoDiarioDetalhesComponent } from 'src/app/componentes/diaria/rendimento-diario-detalhes/rendimento-diario-detalhes.component';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';
import { AplicativoService } from 'src/app/servicos/aplicativo/aplicativo.service';
import { TextMaskModule } from 'angular2-text-mask';
import { DiariaService } from 'src/app/servicos/diaria/diaria.service';
import { RendimentoDiarioService } from 'src/app/servicos/rendimento-diario/rendimento-diario.service';
import { PaginacaoDiariaComponent } from 'src/app/componentes/diaria/paginacao-diaria/paginacao-diaria.component';

@NgModule({
  declarations: [
    DiariaRouterComponent,
    DiariaPainelComponent,
    DiariaCadastroComponent,
    RendimentoDiarioDetalhesComponent,
    PaginacaoDiariaComponent
  ],
  imports: [
    CommonModule,
    DiariaRoutingModule,
    MatDatepickerModule,
    MatFormFieldModule,
    FormsModule,
    MatNativeDateModule,
    MatInputModule,
    TextMaskModule,
    MatExpansionModule,
    MatTabsModule,
    MatRadioModule
  ],
  providers:[
    {provide: MAT_DATE_LOCALE, useValue: 'pt-br'},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    VeiculoService,
    AplicativoService,
    DiariaService,
    RendimentoDiarioService
  ]
})
export class DiariaModule { }

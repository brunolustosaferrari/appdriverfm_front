import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiariaRouterComponent } from 'src/app/componentes/diaria/diaria-router/diaria-router.component';
import { AutenticacaoGuard } from 'src/app/autenticacao.guard';
import { DiariaPainelComponent } from 'src/app/componentes/diaria/diaria-painel/diaria-painel.component';
import { DiariaCadastroComponent } from 'src/app/componentes/diaria/diaria-cadastro/diaria-cadastro.component';

const routes: Routes = [
  {path: "",
   component: DiariaRouterComponent,
   canActivate: [AutenticacaoGuard],
   canActivateChild: [AutenticacaoGuard],
   children:[
     { path: "", component: DiariaPainelComponent},
     { path: "cadastrar", component: DiariaCadastroComponent},
      { path: "atualizar/:id", component: DiariaCadastroComponent}
   ]
 },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiariaRoutingModule { }

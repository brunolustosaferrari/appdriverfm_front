import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MAT_DATE_LOCALE, MatNativeDateModule, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { TextMaskModule } from 'angular2-text-mask';
import {MatPaginatorModule} from '@angular/material/paginator';

import { CustoRoutingModule } from './custo-routing.module';
import { CustoRouterComponent } from 'src/app/componentes/custo/custo-router/custo-router.component';
import { CustoPainelComponent } from 'src/app/componentes/custo/custo-painel/custo-painel.component';
import { CustoCadastroComponent } from 'src/app/componentes/custo/custo-cadastro/custo-cadastro.component';
import { ParcelaCadastroComponent } from 'src/app/componentes/custo/parcela-cadastro/parcela-cadastro.component';
import { CustoDetalhesComponent } from 'src/app/componentes/custo/custo-detalhes/custo-detalhes.component';
import { CustoService } from 'src/app/servicos/custo/custo.service';
import { PaginacaoCustoComponent } from 'src/app/componentes/custo/paginacao-custo/paginacao-custo.component';
import { VeiculoService } from 'src/app/servicos/veiculo/veiculo.service';
import { TipoCustoService } from 'src/app/servicos/custo/tipo-custo.service';
import { FormsModule } from '@angular/forms';
import { TipoCustoCadastroComponent } from 'src/app/componentes/custo/tipo-custo-cadastro/tipo-custo-cadastro.component';
import { ParcelaEmAbertoListaComponent } from 'src/app/componentes/custo/parcela-em-aberto-lista/parcela-em-aberto-lista.component';
import { ParcelaService } from 'src/app/servicos/custo/parcela.service';



@NgModule({
  declarations: [
    CustoRouterComponent,
    CustoPainelComponent,
    CustoCadastroComponent,
    ParcelaCadastroComponent,
    CustoDetalhesComponent,
    PaginacaoCustoComponent,
    TipoCustoCadastroComponent,
    ParcelaEmAbertoListaComponent
  ],
  imports: [
    CommonModule,
    CustoRoutingModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    TextMaskModule,
    MatPaginatorModule
  ],
  entryComponents:[
      TipoCustoCadastroComponent
  ],
  providers:[
    CustoService,
    VeiculoService,
    TipoCustoService,
    ParcelaService,
    {provide: MAT_DATE_LOCALE, useValue: 'pt-br'},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ]
})
export class CustoModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustoRouterComponent } from 'src/app/componentes/custo/custo-router/custo-router.component';
import { AutenticacaoGuard } from 'src/app/autenticacao.guard';
import { CustoPainelComponent } from 'src/app/componentes/custo/custo-painel/custo-painel.component';
import { CustoCadastroComponent } from 'src/app/componentes/custo/custo-cadastro/custo-cadastro.component';
import { ParcelaEmAbertoListaComponent } from 'src/app/componentes/custo/parcela-em-aberto-lista/parcela-em-aberto-lista.component';

const routes: Routes = [
  {path: "",
   component: CustoRouterComponent,
   canActivate: [AutenticacaoGuard],
   canActivateChild: [AutenticacaoGuard],
   children:[
     { path: "", component: CustoPainelComponent},
     { path: "cadastrar", component: CustoCadastroComponent},
     { path: "atualizar/:id", component: CustoCadastroComponent},
     { path: "em-aberto", component: ParcelaEmAbertoListaComponent}
   ]
 },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustoRoutingModule { }

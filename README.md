# AppdriverfmFront

Front end para o projeto AppDriverFm, objetiva permitir ao motorista de aplicativo controle de seus ganhos e custos. Desenvolvido usando Angular. A url para a api deve ser
definida em enviroments/enviroment.ts. Este é um projeto em desenvolvimento, faltam requisitos funcionais de geração de relatórios e usabilidade em geral; por hora só é
possível lançar dados e gerar relatórios de faturamento.

## Como utilizar

O usuário deve cadastrar os veículos e os aplicativos que utiliza em seu trabalho. Em "Diárias" o mesmo pode lançar os seus ganhos em um dia de trabalho, para cada par 
aplicativo / veículo deve ser lançado o valor recebido no dia, os quilômetros corridos com o veiculo para o aplicativo, o tempo em corridas e o valor recebido do 
aplicativo, a fim de serem utilizados para comparação nos relatórios. Em "Custos" o usuário de lançar os gastos de um veículo, devendo especificar um tipo, uma data
de ocorrência - que visa identificar quando o custo teve origem e consequentemente calcular quanto o veículo gera de custo por km em utilização - e as parcelas para
pagamento, as quais podem ser visualizadas a partir do painel de custos e marcadas como pagas - o objetivo é acompanhar o que deve ser pago no futuro.